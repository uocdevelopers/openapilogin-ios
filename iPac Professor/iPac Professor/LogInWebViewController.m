//
//  ViewController.m
//
//  Created by Miguel on 23/10/14.
//  Copyright (c) 2014 uoc. All rights reserved.
//

#import "LogInWebViewController.h"

//Certificate stuff
@implementation NSURLRequest(DataController)
+ (BOOL)allowsAnyHTTPSCertificateForHost:(NSString *)host
{
    return YES;
}
@end

@interface LogInWebViewController ()

@end

@implementation LogInWebViewController {
    DataController* data;
    UIAlertController *alertView;
}



- (void)viewDidLoad {
    [super viewDidLoad];
    [self configure];
       // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) configure {
    [self configureDataController];
    [self configureWebView];
    [self configureAlertView];
}

-(void) configureDataController {
    data = [DataController sharedInstance];
    data.delegate = self;

}

- (void) configureWebView {
    _webView.delegate = [data loginWebViewDelegate];
    NSURLRequest* req = [data requestToInitWebView];
    [_webView loadRequest:req];
}

- (void) configureAlertView {
    alertView = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"authorizeErrorAlertTitle", nil) message:NSLocalizedString(@"authorizeErrorAlertMessage", nil) preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *confirm = [UIAlertAction actionWithTitle:NSLocalizedString(@"authorizeErrorAlertConfirmationMessage", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction * action){
        [alertView dismissViewControllerAnimated:YES completion:nil];
        [_webView  loadRequest:[data requestToRetry]];
    }];
    [alertView addAction:confirm];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"authorizeErrorAlertCancelMessage", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
        [alertView dismissViewControllerAnimated:YES completion:nil];
        [_webView loadRequest:[data requestToCancel]];
    }];
    [alertView addAction:cancel];
}


#pragma mark - DataController delegate

- (void) dataController:(DataController *)dataController receivedNetworkError:(NSString *)error {
    NSLog(@"error:%@",error);
    
    if ([error isEqualToString:GENERALNETWORKERROR]) [_webView loadRequest:[data requestToRestartWebView]];
    else if ([error isEqualToString:SERVERERROR]) [self presentViewController:alertView animated:YES completion:nil];
}



@end
