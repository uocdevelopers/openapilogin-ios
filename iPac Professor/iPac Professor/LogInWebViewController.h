//
//  ViewController.h
//
//  Created by Miguel on 23/10/14.
//  Copyright (c) 2014 uoc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataController.h"

@interface LogInWebViewController : UIViewController <NSURLConnectionDataDelegate, DataControllerDelegate>

@property (weak, nonatomic) IBOutlet UIWebView *webView;

@end

