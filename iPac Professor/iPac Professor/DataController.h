//
//  DataController.h
//
//  Created by Miguel on 24/10/14.
//  Copyright (c) 2014 uoc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#import "PersistenceController.h"
#import "NetworkController.h"

#import "NSURL+StringWithFormat.h"

@protocol DataControllerDelegate;

@interface DataController : NSObject <NetworkControllerDelegate>
+ (id) sharedInstance;

@property(nonatomic,strong) id<UIWebViewDelegate> loginWebViewDelegate;
@property(nonatomic,strong) id<DataControllerDelegate> delegate;

@property(nonatomic) BOOL firstRunEver;

- (NSURLRequest*) requestToInitWebView;
- (NSURLRequest*) requestToRestartWebView;
- (NSURLRequest*) requestToRetry;
- (NSURLRequest*) requestToCancel;

@end



@protocol DataControllerDelegate
@required
- (void) dataController:(DataController *)dataController receivedNetworkError:(NSString *)error;


@optional


@end
