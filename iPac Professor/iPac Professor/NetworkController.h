//
//  NetworkController.h
//
//  Created by Miguel on 23/10/14.
//  Copyright (c) 2014 uoc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constants.h"
@import UIKit;


@protocol NetworkControllerDelegate;




@interface NetworkController : NSObject <UIWebViewDelegate, NSURLConnectionDelegate>

@property (nonatomic, assign) id<NetworkControllerDelegate> delegate;

- (void) deleteCookies;

- (void) interceptQueryKey:(NSString*)queryKey forScheme:(NSString*)scheme;

- (void) requestJSONDataFromURL:(NSURL*)url;
- (void) requestJSONDataFromURL:(NSURL*)url andPOSTParameters:(NSString*)POSTString;


@end




@protocol NetworkControllerDelegate

@required
- (void) networkController:(NetworkController*)networkController receivedGeneralError:(NSString*)error;

@optional
- (void) networkController:(NetworkController*)networkController interceptedValue:(NSString*)value forQueryKey:(NSString*)queryKey;
- (void) networkController:(NetworkController*)networkController recievedJSONData:(NSDictionary*)data fromURL:(NSURL*)url;


@end
