//
//  NetworkController.m
//
//  Created by Miguel on 23/10/14.
//  Copyright (c) 2014 uoc. All rights reserved.
//

#import "NetworkController.h"

@implementation NetworkController {
    NSMutableData *_responseData;
    BOOL interceptMode;
    NSString* interceptScheme;
    NSString* interceptQueryKey;
}
@synthesize delegate;

- (void) requestJSONDataFromURL:(NSURL*)url andPOSTParameters:(NSString*)POSTString {
    NSMutableURLRequest *dataRequest = [[NSMutableURLRequest alloc] initWithURL:url];
    
    if (POSTString != nil) {
        [dataRequest setHTTPMethod:@"POST"];
        [dataRequest setHTTPBody:[POSTString dataUsingEncoding:NSUTF8StringEncoding]];
    }
    [NSURLConnection connectionWithRequest:dataRequest delegate:self];
}

- (void) requestJSONDataFromURL:(NSURL*)url {
    [self requestJSONDataFromURL:url andPOSTParameters:nil];
}


- (void) interceptQueryKey:(NSString*)queryKey forScheme:(NSString*)scheme {
    interceptMode = YES;
    interceptQueryKey = queryKey;
    interceptScheme = scheme;
}


#pragma mark - WebView Delegate
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    if (interceptMode) {
        NSString* query = [[request URL] query];
        if ([[[request URL] scheme] isEqualToString:interceptScheme]) {
            if ([query length] == 0) {
                if (delegate && [(id) delegate respondsToSelector:@selector(networkController:receivedGeneralError:)]) {
                    [delegate networkController:self receivedGeneralError:NOQUERYERROR];
                }
            }
            else {
                NSString *value = [self getValueForKey:interceptQueryKey onQuery:query];
                if (value != nil && [value length] > 0) {
                    if ( delegate && [(id) delegate respondsToSelector:@selector(networkController:interceptedValue:forQueryKey:)]) {
                        [delegate networkController:self interceptedValue:value forQueryKey:interceptQueryKey];
                    }
                    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                    return NO;
                }
                else {
                    if (delegate && [(id) delegate respondsToSelector:@selector(networkController:receivedGeneralError:)]) {
                        [delegate networkController:self receivedGeneralError:NOQUERYERROR];
                    }
                }
            }
        }
    }
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    return YES;
}


-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    NSLog(@"error!!!!! :%@",error);
}

-(void)webViewDidFinishLoad:(UIWebView *)webView {
    //Guarrada per omplir lusuari i la pass
    [webView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"javascript:document.getElementById('username').value = '%@';document.getElementById('password').value='%@';", @"chintzmann", @"campus25"]];
    
    //Guarrada per veure si hi ha error
    NSString *theTitle=[webView stringByEvaluatingJavaScriptFromString:@"document.title"];
    if ([theTitle isEqualToString:SERVERERROR]) {
        if (delegate && [(id) delegate respondsToSelector:@selector(networkController:receivedGeneralError:)]) {
            [delegate networkController:self receivedGeneralError:SERVERERROR];
        }
    }
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
}


#pragma mark - Cookie stuff

- (void) deleteCookies {
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    NSHTTPCookieStorage *cookieStorage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    for (NSHTTPCookie *cookie in cookieStorage.cookies) {
        [cookieStorage deleteCookie:cookie];
    }
}

#pragma mark - aux
-(NSString *) getValueForKey:(NSString*)key onQuery:(NSString*)query {
    //massa engorros pel que vull? obtengo un parametro de una query html
    NSMutableDictionary *queryStringDictionary = [[NSMutableDictionary alloc] init];
    NSArray *urlComponents = [query componentsSeparatedByString:@"&"];
    
    for (NSString *keyValuePair in urlComponents)
    {
        NSArray *pairComponents = [keyValuePair componentsSeparatedByString:@"="];
        if ([pairComponents count] > 1) {
            NSString *key = [pairComponents objectAtIndex:0];
            NSString *value = [pairComponents objectAtIndex:1];
            [queryStringDictionary setValue:value forKey:key];
        }
        
    }
    return [queryStringDictionary objectForKey:key];
}

#pragma mark - NSURLConnection delegate methods


- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    // A response has been received, this is where we initialize the instance var you created
    // so that we can append data to it in the didReceiveData method
    // Furthermore, this method is called each time there is a redirect so reinitializing it
    // also serves to clear it
    _responseData = [[NSMutableData alloc] init];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    // Append the new data to the instance variable you declared
    [_responseData appendData:data];

}

- (NSCachedURLResponse *)connection:(NSURLConnection *)connection
                  willCacheResponse:(NSCachedURLResponse*)cachedResponse {
    // Return nil to indicate not necessary to store a cached response for this connection
    return nil;
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    // The request is complete and data has been received
    // You can parse the stuff in your instance variable now
    
    if (delegate && [(id) delegate respondsToSelector:@selector(networkController:recievedJSONData:fromURL:)]) {
        NSURL* originalURL = [[connection originalRequest] URL];
        NSDictionary *JSONData = [NSJSONSerialization JSONObjectWithData:_responseData options:0 error:nil];
        [delegate networkController:self recievedJSONData:JSONData fromURL:originalURL];
    }

}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    // The request has failed for some reason!
    // Check the error var
}

@end
