//
//  NSURL+StringWithFormat.m
//
//  Created by Miguel on 13/11/14.
//  Copyright (c) 2014 uoc. All rights reserved.
//

#import "NSURL+StringWithFormat.h"

@implementation NSURL (StringWithFormat)

+ (NSURL*) URLFromStringWithFormat:(NSString*)string,... {
    va_list args;
    va_start(args, string);
    return [NSURL URLWithString:[[NSString alloc] initWithFormat:string arguments:args]];
    va_end(args);
}

@end
