//
//  PersistenceController.m
//
//  Created by Miguel on 23/10/14.
//  Copyright (c) 2014 uoc. All rights reserved.
//

#import "PersistenceController.h"

@implementation PersistenceController
@synthesize token,refreshToken,userClientID,userSecret,tokenExpiresIn,session,sessionExpiresIn,photoURL;

- (id)init
{
    self = [super init];
    
    if (self) {
        // init values
    }
    
    return self;
}

#pragma mark - load
-(void) load {
    [self loadSensitiveData];
    [self loadNonSensitiveData];
}

- (void) loadSensitiveData {
    PDKeychainBindings *bindings = [PDKeychainBindings sharedKeychainBindings];
    token = [bindings objectForKey:@"token"];
    refreshToken = [bindings objectForKey:@"refreshToken"];
    userClientID = [bindings objectForKey:@"userClientID"];
    userSecret = [bindings objectForKey:@"userSecret"];
    session = [bindings objectForKey:@"session"];
    //care
    photoURL = [NSURL URLWithString:[bindings objectForKey:@"photoURL"]];

}

- (void) loadNonSensitiveData {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    tokenExpiresIn = [defaults objectForKey:@"tokenExpiresIn"];
    sessionExpiresIn = [defaults objectForKey:@"sessionExpiresIn"];
}

#pragma mark - save


- (void) save {
    [self saveSensistiveData];
    [self saveNonSensitiveData];
}



- (void) saveSensistiveData {
    PDKeychainBindings *bindings = [PDKeychainBindings sharedKeychainBindings];
    if (token) [bindings setObject:token forKey:@"token"];
    if (refreshToken) [bindings setObject:refreshToken forKey:@"refreshToken"];
    if (userClientID) [bindings setObject:userClientID forKey:@"userClientID"];
    if (userSecret) [bindings setObject:userSecret forKey:@"userSecret"];
    if (session) [bindings setObject:session forKey:@"session"];
    //care, only strings in KeyChain
    if (photoURL) {
        [bindings setObject:[photoURL absoluteString] forKey:@"photoURLString"];
    }
}

- (void) saveNonSensitiveData {
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    if (tokenExpiresIn) [defaults setObject:defaults forKey:@"tokenExpiresIn"];
    if (sessionExpiresIn) [defaults setObject:sessionExpiresIn forKey:@"sessionExpiresIn"];
    
}

#pragma mark - clear

- (void) clear {
    [self clearSensitiveData];
    [self clearNonSensitiveData];
}

- (void) clearSensitiveData {
    PDKeychainBindings *bindings = [PDKeychainBindings sharedKeychainBindings];
    token = nil;
    refreshToken = nil;
    userClientID = nil;
    userSecret = nil;
    session = nil;
    photoURL = nil;
    if ([bindings objectForKey:@"token"]) [bindings removeObjectForKey:@"token"];
    if ([bindings objectForKey:@"refreshToken"]) [bindings removeObjectForKey:@"refreshToken"];
    if ([bindings objectForKey:@"userClientID"]) [bindings removeObjectForKey:@"userClientID"];
    if ([bindings objectForKey:@"userSecret"]) [bindings removeObjectForKey:@"userSecret"];
    if ([bindings objectForKey:@"session"]) [bindings removeObjectForKey:@"session"];
    if ([bindings objectForKey:@"photoURL"]) [bindings removeObjectForKey:@"photoURL"];
}



- (void) clearNonSensitiveData {
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    tokenExpiresIn = nil;
    sessionExpiresIn = nil;
    if ([defaults objectForKey:@"tokenExpiresIn"]) [defaults removeObjectForKey:@"tokenExpiresIn"];
    if ([defaults objectForKey:@"sessionExpiresIn"]) [defaults removeObjectForKey:@"sessionExpiresIn"];
}


@end
