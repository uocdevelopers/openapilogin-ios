//
//  NSURL+StringWithFormat.h
//
//  Created by Miguel on 13/11/14.
//  Copyright (c) 2014 uoc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSURL (StringWithFormat)

+ (NSURL*) URLFromStringWithFormat:(NSString*)string,...;
@end
