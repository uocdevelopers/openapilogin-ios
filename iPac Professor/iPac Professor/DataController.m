//
//  DataController.m
//
//  Created by Miguel on 24/10/14.
//  Copyright (c) 2014 uoc. All rights reserved.
//

#import "DataController.h"



enum ExpectedDataTypes : NSUInteger {
    ExpectingNothing,
    ExpectingTokenData,
    ExpectingAPPCredentialsData,
    ExpectingUserData
} expectingData; //Set this before every call to the networkController

@implementation DataController {
    PersistenceController* persistence;
    NetworkController* network;
    NSString* deviceName;
    id<NetworkControllerDelegate> netWorkControllerDelegate;
}

static DataController *sharedInstance = nil;
@synthesize loginWebViewDelegate,delegate, firstRunEver;

+ (DataController *)sharedInstance {
    if (nil != sharedInstance) {
        return sharedInstance;
    }
    static dispatch_once_t pred;        // Lock
    dispatch_once(&pred, ^{             // This code is called at most once per app
        sharedInstance = [[self alloc] init];
    });
    
    return sharedInstance;
}

- (id)init
{
    self = [super init];
    
    if (self) {
        expectingData = ExpectingNothing;
        //aixo no se si hauria d'anar aqui
        persistence = [[PersistenceController alloc] init];
        network = [[NetworkController alloc] init];
        [network deleteCookies];
        
        //We want the network Controller to intercept the code in a specific request so we can use it to get the token :)
        [network interceptQueryKey:CODEKEY forScheme:REDIRECTSCHEME];
        network.delegate = self;
        deviceName = [[[UIDevice currentDevice] name] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    }
    
    return self;
}

- (id)copyWithZone:(NSZone *)zone {
    return self;
}

- (void) setFirstRunEver:(BOOL)firstRunEver {
    [persistence clear];
}

- (id<UIWebViewDelegate>) loginWebViewDelegate {
    return network;
}

-(NSURLRequest*) requestToInitWebView {
    return [self requestToAuthorizeWithClientID:CLIENTID andRedirectURI:REDIRECTURI];
}

- (NSURLRequest*) requestToRestartWebView {
    [network deleteCookies];
    return [self requestToInitWebView];
}

-(NSURLRequest*) requestToRetry {
    return [self requestToInitWebView];
}

-(NSURLRequest*) requestToCancel {

    
    return [self requestToRestartWebView];
}


-(NSURLRequest*) requestToAuthorizeWithClientID:(NSString*)clientID andRedirectURI:(NSString*)redirectURI {
    NSString* authorizeURLString = [NSString stringWithFormat:AUTHURI,deviceName,clientID,redirectURI];
    NSURL* reqURL = [NSURL URLWithString:authorizeURLString];
    NSURLRequest* req = [NSURLRequest requestWithURL:reqURL];
    return req;
}

#pragma mark - Persistence DATA Calls

- (void) saveTokenData:(NSDictionary*)data {
    
    NSString *token = [data objectForKey:ACCESSTOKENKEY];
    if (token && [token length] > 0) {
        NSLog(@"TOKEN:%@",token);
        NSString *refreshToken = [data objectForKey:REFRESHTOKENKEY];
        NSString *tokenExpirationSecondsString= [data objectForKey:EXPIRESINKEY];
        unsigned long long int tokenExpirationSeconds = [tokenExpirationSecondsString intValue];
        NSDate *expiresIn = [NSDate dateWithTimeIntervalSinceNow:tokenExpirationSeconds];
        NSLog(@"EXPIRES IN:%@",expiresIn);
        [persistence setToken:token];
        [persistence setRefreshToken:refreshToken];
        [persistence setTokenExpiresIn:expiresIn];
        if (WANTSPERSISTENCE) [persistence save];
        
    }
    else {
        [self notifyNetworkError:GENERALNETWORKERROR];
    }
}

- (void) saveAPPCredentialsData:(NSDictionary*)data {
    NSString *client = [data objectForKey:CLIENTKEY];
    if (client && [client length] > 0) {
        NSString* secret = [data objectForKey:SECRETKEY];
        [persistence setUserClientID:client];
        [persistence setUserSecret:secret];
        if (WANTSPERSISTENCE) [persistence save];
    }
    
    else [self notifyNetworkError:GENERALNETWORKERROR];
}

- (void) saveUserData:(NSDictionary*)data {
    NSString *session = [data objectForKey:SESSIONKEY];
    if (session && [session length] > 0) {
        NSString* photoURLString = [data objectForKey:PHOTOURLKEY];
        NSURL* photoURL = [NSURL URLWithString:photoURLString];
        NSDate* expiresIn = [NSDate dateWithTimeIntervalSinceNow:SESSIONEXPIRATIONSECONDS];
        [persistence setSession:session];
        [persistence setPhotoURL:photoURL];
        [persistence setSessionExpiresIn:expiresIn];
        if (WANTSPERSISTENCE) [persistence save];
    }
    else [self notifyNetworkError:GENERALNETWORKERROR];
}


#pragma mark - Network DATA calls

//OPEN API
- (void) requestTokenDataWithCode:(NSString*)code {
    NSURL* tokenURL = [NSURL URLWithString:TOKENURI];
    NSString* POSTString = [NSString stringWithFormat:TOKENPOSTPARAMETERS,code];
    expectingData = ExpectingTokenData;
    [network requestJSONDataFromURL:tokenURL andPOSTParameters:POSTString];
}

- (void) requestAPPCredentialsWithToken:(NSString*)token {
    expectingData = ExpectingAPPCredentialsData;
    [network requestJSONDataFromURL:[NSURL URLFromStringWithFormat:APPCREDENTIALSURI,token]];
}

- (void) requestUserDataWithToken:(NSString*)token {
    expectingData = ExpectingUserData;
    [network requestJSONDataFromURL:[NSURL URLFromStringWithFormat:USERDATAURI,token]];
}


#pragma mark - NetworkController Delegate

- (void)networkController:(NetworkController *)networkController interceptedValue:(NSString *)value forQueryKey:(NSString *)queryKey {
    if ([queryKey isEqualToString:CODEKEY]) {
        [self requestTokenDataWithCode:value];
    }
}

- (void) networkController:(NetworkController*)networkController receivedGeneralError:(NSString*)error {
    if (![error isEqualToString:SERVERERROR]) error = GENERALNETWORKERROR;
    [self notifyNetworkError:error];
}

- (void) networkController:(NetworkController*)networkController recievedJSONData:(NSDictionary*)data fromURL:(NSURL*)url {
    NSLog(@"recieved data:%@ from url:%@",data,url);
    
    if (data) {
        
        switch (expectingData) {
            case ExpectingTokenData: {
                [self handleRecievedTokenData:data];
            }
                break;
            case ExpectingAPPCredentialsData: {
                [self handleRecievedAPPCredentialsData:data];
            }
                break;
            case ExpectingUserData: {
                [self handleRecievedUserData:data];
            }
                break;
            default: {
            }
                break;
        }
    }
    else [self notifyNetworkError:GENERALNETWORKERROR];
    expectingData = ExpectingNothing;
}

#pragma mark - DELEGATE CALLBACKS

- (void) notifyNetworkError:(NSString*)error {
    if (delegate && [(id) delegate respondsToSelector:@selector(dataController:receivedNetworkError:)]) {
        [delegate dataController:self receivedNetworkError:error];
    }
}

#pragma mark - Handlers

- (void) handleRecievedTokenData:(NSDictionary*) data {
    [self saveTokenData:data];
    [self requestAPPCredentialsWithToken:[persistence token]];
}

- (void) handleRecievedAPPCredentialsData:(NSDictionary*) data {
    [self saveAPPCredentialsData:data];
    [self requestUserDataWithToken:[persistence token]];
}

- (void) handleRecievedUserData:(NSDictionary*) data {
    [self saveUserData:data];
    //NOW WE HAVE EVERYTHING TO START
}

@end
