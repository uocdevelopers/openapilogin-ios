//
//  PersistenceController.h
//
//  Created by Miguel on 23/10/14.
//  Copyright (c) 2014 uoc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PDKeychainBindings.h"

@interface PersistenceController : NSObject

@property (nonatomic, strong) NSString *token;
@property (nonatomic, strong) NSString *refreshToken;
@property (nonatomic, strong) NSString *userClientID;
@property (nonatomic, strong) NSString *userSecret;
@property (nonatomic, strong) NSDate* tokenExpiresIn;

@property (nonatomic, strong) NSString *session;
@property (nonatomic, strong) NSDate* sessionExpiresIn;

@property (nonatomic, strong) NSURL *photoURL;


- (void) save;
- (void) load;
- (void) clear;

@end
