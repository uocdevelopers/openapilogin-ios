//
//  Constants.h
//
//  Created by Miguel on 23/10/14.
//  Copyright (c) 2014 uoc. All rights reserved.
//

//OPTIONS Delete when decided

#define WANTSPERSISTENCE 0

//BASE

#define BASEURI @"http://cv-pre.uoc.edu/webapps"

//#define BASEURI @"http://cv.uoc.edu/webapps"


//OpenAPI Login

#define UOCAPI BASEURI @"/uocapi"

#define OAUTH UOCAPI @"/oauth"

#define AUTHURI OAUTH @"/authorize?device=%@&client_id=%@&redirect_uri=%@&response_type=code"

#define TOKENURI OAUTH @"/token?"


//LOGIN aux

#define REDIRECTSCHEME @"ipacprofessor"

#define REDIRECTURI REDIRECTSCHEME @"://"

#define CODEKEY @"code"

#define TOKENPOSTPARAMETERS @"client_id=" CLIENTID "&client_secret=" SECRET "&grant_type=authorization_code&redirect_uri=" REDIRECTURI "&code=%@"


// UOCAPI

#define APIURI UOCAPI @"/api/v1"
#define APPCREDENTIALSURI APIURI @"/app?access_token=%@"
#define USERDATAURI APIURI @"/user?access_token=%@"

// UOCAPI KEYS


//Token Data
#define ACCESSTOKENKEY @"access_token"
#define REFRESHTOKENKEY @"refresh_token"
#define EXPIRESINKEY @"expires_in"
#define SESSIONKEY @"sessionId"
#define PHOTOURLKEY @"photoUrl"

//App credentials Data

#define CLIENTKEY @"client"
#define SECRETKEY @"secret"

#define SESSIONEXPIRATIONMINUTES 80 - SESSIONEXPIRATIONERRORMARGINMINUTES
#define SESSIONEXPIRATIONSECONDS SESSIONEXPIRATIONMINUTES * 60

#define SESSIONEXPIRATIONERRORMARGINMINUTES 2

//ERRORS

#define SERVERERROR @"Error del servidor"

#define NOQUERYERROR @"No query on request URL"

#define GENERALNETWORKERROR @"General network error"


//CLIENTID & SECRET

#define CLIENTID @"CnCluRvS5AlOxeZ3MG2znfOajJz0GtT9EyYQPZ0qCZZijLyYGLpaa7B2HZl8hUaMieNLj1PXkSrGe8Y0F4LgppQC2WNv6hwgu87D0w2LTBS4Dzgcf9ut4KYmv7BVF5Qw"

#define SECRET @"gVqavnNrD4jHQ7q1DmywVqvZQ2Hz68bTsHkgQ1Cbge8MD5kH3hAFEJ0sD8sJKOwOOBhKm9PqAWO7m8FLsgyusUm1D88lMtgIsJWtHFdHBv22bnmjkm3vyZdOmik0TDrj"